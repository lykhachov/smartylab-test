<?php

class gitAPI
{
    private $http;

    private $replace = array(
        'equal'   => '',
        'less'    => '<',
        'larger' => '>'
    );

    const gitApiUrl = 'https://api.github.com/search/repositories?sort=stars&order=desc&q=';

    /**
     * gitAPI constructor.
     * @param $guzzle GuzzleHttp\ClientInterface
     */
    public function __construct($guzzle)
    {
        $this->http = new $guzzle([
            'debug' => false
        ]);
    }

    public function getRepositories($conditions)
    {
        $queryString = $this->getQueryString($conditions);
        $result = $this->http->get(self::gitApiUrl . $queryString);
        return json_decode($result->getBody(), true);
//        return $result->getBody();
    }

    /**
     * @param $data array
     * @return string
     */
    private function getQueryString($data)
    {
        $conditions = '';
        foreach ($data as $row) {
            $conditions .= $row['field'] . ':' . $this->replace[$row['operator']] . $row['value'] . '+';
        }
        return $conditions = substr($conditions, 0,-1);
    }
}
<?php
require_once 'inc/vendor/autoload.php';
require_once 'inc/gitAPI.php';

$data = json_decode(file_get_contents('php://input'), true);

$gitAPI = new gitAPI(GuzzleHttp\Client::class);

$result = $gitAPI->getRepositories($data);
print(json_encode($result));
die;
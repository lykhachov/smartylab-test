$(() => {
    let row = $('.query');

    $('#form').on('click', '[name="add"]', (e) => {
        $(row).clone().appendTo('.rows');
    });

    $('#form').on('click', '[name="delete"]', (e) => {
        if ($('.query').length < 2) return;
        $(e.currentTarget).closest('.row').remove();
    });

    $('#form').on('click', '[name="clear"]', (e) => {
        $('.query').remove();
        $(row).clone().appendTo('.rows');
    });

    $('#form').on('click', '[name="apply"]', (e) => {
        let data = [];
        $('#form .query').each((i, el) => {
            let fieldGroup = {};
            $(el).find('.form-control').each((i, subEl) => {
                fieldGroup[subEl.name] = subEl.value;
            });
            data.push(fieldGroup);
        });
        console.log(data);
        data = JSON.stringify(data);
        $.post(
            'ajax.php',
            data,
            null,
            'json'
        ).done((result) => {
            console.log(result)
        }).fail((error) => {
            console.log(error);
        });
    });
});
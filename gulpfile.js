const gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create(),
    yargs = require('yargs'),
    cleanCss = require('gulp-clean-css'),
    gulpif = require('gulp-if'),
    postcss = require('gulp-postcss'),
    newer = require('gulp-newer');

//To test the production: gulp hello --prod=true
const PRODUCTION = yargs.argv.prod;
const buildFolder = 'prod/';
const srcFolder = 'dev/';
const path = {
    build: {
        js: buildFolder + 'js/',
        style: buildFolder + 'css/',
        root: buildFolder,
        img: buildFolder,
        fonts: buildFolder + 'fonts/',
        html: buildFolder
    },
    src: {
        js: srcFolder + 'js/*.js',
        // jsLib: srcFolder + 'js/libs.js',
        style: srcFolder + 'scss/*.scss',
        styleLib: srcFolder + 'scss/style-libs.css',
        img: srcFolder + '**/**/*.{JPG,jpg,png,gif,svg}',
        fonts: srcFolder + 'fonts/**/*.*',
        html: srcFolder + '**/*.html',
        php: srcFolder + '**/*.{php,tpl,json}',
        wpStyleFile: srcFolder + 'style.css',
        wpEditorStyleFile: srcFolder + 'editor-style.scss'
    },
    watch: {
        js: srcFolder + 'js/**/*.js',
        style: srcFolder + 'scss/**/*.scss',
        img: srcFolder + 'img/**/*.*',
        fonts: srcFolder + 'fonts/**/*.*',
        html: srcFolder + '**/*.html',
        php: srcFolder + '**/*.{php,tpl}',
        editorStyle: srcFolder + 'editor-style.scss'
    }
};

//testing production variable
gulp.task('hello', function (cb) {
    console.log(PRODUCTION);
    cb();
});
gulp.task('html', function () {
    return gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
});
gulp.task('php', function () {
    return gulp.src(path.src.php)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.root))
});
gulp.task('wp-main-css', function () {
    return gulp.src(path.src.wpStyleFile)
        .pipe(gulp.dest(path.build.root))
});
gulp.task('wp-editor-css', function () {
    return gulp.src(path.src.wpEditorStyleFile)
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(gulp.dest(path.build.style))
});
gulp.task('css-libs', function () {
    return gulp.src(path.src.styleLib)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.style))
});

gulp.task('js-libs', function () {
    return gulp.src(path.src.jsLib)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
});
gulp.task('js', function () {
    return gulp.src(path.src.js)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(rigger())
        // .pipe(babel({presets: ['env']}))
        // .pipe(uglify())
        .pipe(gulpif(PRODUCTION, uglify()))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(gulp.dest(path.build.js))
        .pipe(gulpif(PRODUCTION, browserSync.stream()));
});
gulp.task('css', function () {
    return gulp.src(path.src.style)
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(gulpif(PRODUCTION, cleanCss({compatibility: 'ie8'})))
        // .pipe(prefixer())
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        // .pipe(cssmin())
        .pipe(gulp.dest(path.build.style))
        .pipe(gulpif(!PRODUCTION, browserSync.stream()));
});
gulp.task('imageMin', function () {
    return gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});
gulp.task('fonts', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('watcher', function () {
    gulp.watch(path.watch.style).on('change', gulp.series('css'));
    gulp.watch(path.watch.js).on('change', gulp.series('js'));
    gulp.watch(path.watch.php).on('change', gulp.series('php'));
    gulp.watch(path.watch.img).on('change', gulp.series('imageMin'));
    gulp.watch(path.watch.img).on('change', gulp.series('imageMin'));
    gulp.watch(path.watch.editorStyle).on('change', gulp.series('wp-editor-css'));
});
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./prod/"
        }
    });
});
//npm run start for dev and npm run build for build
gulp.task('default', gulp.series(
    gulp.parallel('css-libs', 'css', 'js', 'php', 'html'),
    gulp.parallel('watcher')
));
/*to run build task from gulp tasks hover on it, click RMB -> edit 'build' settings -> Arguments: --prod
or just: npm run build
*/
gulp.task('build', gulp.series(
    gulp.parallel('imageMin', 'css-libs', 'js-libs', 'css', 'js', 'fonts', 'php', 'wp-main-css', 'wp-editor-css'),
));
// gulp.task('default', gulp.series(
//   gulp.parallel('imageMin', 'css-libs', 'js-libs', 'css', 'js', 'fonts', 'php', 'wp-main-css'),
//   gulp.parallel('watcher')
// ));
